﻿using MusicApp.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Text;

namespace MusicApp.Services
{
	public class ApiCaller
	{
		public ObservableCollection<Music> Call()
		{
            var list = new ObservableCollection<Music>();
            var client = new RestClient("https://deezerdevs-deezer.p.rapidapi.com/search?q=linkinpark");
            var request = new RestRequest(Method.GET);
            request.AddHeader("x-rapidapi-key", "676e7af180msh8ca19e20037baf5p10cee7jsn56ff80f4b8d6");
            request.AddHeader("x-rapidapi-host", "deezerdevs-deezer.p.rapidapi.com");
            IRestResponse response = client.Execute(request);
            var output = JObject.Parse(response.Content);
            var some = output["data"];
            foreach(var item in some)
			{
                var music = new Music { 
                    Artist = (string)item["artist"]["name"],
                    CoverImage = (string)item["album"]["cover_medium"],
                    Url=(string)item["preview"],
                    Title = (string)item["title_short"]
                };
                list.Add(music);
			}
            list[0].IsRecent = true;
            return list;
        }
	}
}
